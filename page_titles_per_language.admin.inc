<?php
/**
 * @file
 * Admin include file.
 */

/**
 * Return a form to edit page titles per language.
 */
function page_titles_per_language_form() {

  $general_form = _page_titles_per_language_general();
  $types_form = _page_titles_per_language_types();
  $vocab_form = _page_titles_per_language_vocab();
  $form = $general_form + $types_form + $vocab_form;

  if (module_exists('vertical_tabs')) {
    $form['#pre_render'][] = 'vertical_tabs_form_pre_render';
  }

  return system_settings_form($form);
}

/**
 * Return a form for general page titles.
 */
function _page_titles_per_language_general() {
  // Define default looking 'form elements' for setting.
  $pattern_form_element = array(
    '#type' => 'textfield',
    '#size' => 50,
    '#maxlength' => 256,
  );

  // Initialisation of the general form
  $form['general'] = array(
    '#title' => t('General'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#weight' => -10,
    '#theme' => 'page_titles_per_language_admin_settings',
  );

  // List of all enabled languages
  $langs = locale_language_list($field = 'name', $all = FALSE);

  // Initialisation of general and specific settings of patterns
  $patterns = _page_titles_per_language_init_general_patterns_options();

  // Building the general form
  if (!empty($langs)) {
    foreach ($patterns as $key => $pattern) {
      // generation of all patterns for each languages
      foreach ($langs as $langcode => $langname) {
        $lang_key = empty($langcode) ? $key : $key . '_' . $langcode;
        if ((isset($pattern['#langneutral']) && $pattern['#langneutral'] == 1) || ($langcode != '')) {
          $form['general']['pattern'][$lang_key] = array() + $pattern_form_element;
          $form['general']['pattern'][$lang_key]['#title'] = $pattern['#title'];
          $form['general']['scope'][$lang_key] = array('#type' => 'markup', '#value' => $pattern['#scope']);
          $form['general']['pattern'][$lang_key]['#default_value'] = variable_get($lang_key, '');
          $form['general']['language'][$lang_key] = array('#type' => 'markup', '#value' => t('%lang', array('%lang' => $langname)));
        }
      }
    }
  };

  // Token help
  _page_titles_per_language_add_tokens_help($form, 'general');

  return $form;
}

/**
 * Return a form for content types page titles.
 */
function _page_titles_per_language_types() {
  // Define default looking 'form elements' for setting.
  $pattern_form_element = array(
    '#type' => 'textfield',
    '#size' => 50,
    '#maxlength' => 256,
  );

  // Initialisation of the content types form
  $form['types'] = array(
    '#title' => t('Content Types'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#weight' => -9,
    '#theme' => 'page_titles_per_language_admin_settings',
  );

  // List of all enabled languages
  $langs = locale_language_list($field = 'name', $all = FALSE);

  // Building the content types form
  $types = node_get_types('names');
  foreach ($types as $node_type => $node_type_name) {
    $key = 'page_title_type_' . $node_type;
    foreach ($langs as $langcode => $langname) {
      $lang_key = empty($langcode) ? $key : $key . '_' . $langcode;
      $form['types']['pattern'][$lang_key] = array() + $pattern_form_element;
      $form['types']['pattern'][$lang_key]['#title'] = t('%title', array('%title' => $node_type_name));
      $form['types']['scope'][$lang_key] = array('#type' => 'markup', '#value' => t('Node'));
      $form['types']['pattern'][$lang_key]['#default_value'] = variable_get($lang_key, '');
      $form['types']['language'][$lang_key] = array('#type' => 'markup', '#value' => t('%lang', array('%lang' => $langname)));
    }
  }

  // Token help
  _page_titles_per_language_add_tokens_help($form['types'], 'types');

  return $form;
}

/**
 * Return a form for taxonomy page titles.
 */
function _page_titles_per_language_vocab() {
  // Define default looking 'form elements' for setting.
  $pattern_form_element = array(
    '#type' => 'textfield',
    '#size' => 50,
    '#maxlength' => 256,
  );

  // Initialisation of the taxonomy form
  $form['taxonomy'] = array(
    '#title' => t('Taxonomy'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#weight' => -8,
    '#theme' => 'page_titles_per_language_admin_settings',
  );

  // List of all enabled languages
  $langs = locale_language_list($field = 'name', $all = FALSE);

  // Building the taxonomy form
  if (module_exists('taxonomy')) {
    $vocabularies = taxonomy_get_vocabularies();
    if (sizeof($vocabularies) > 0) {
      foreach ($vocabularies as $vocab) {
        $key = 'page_title_vocab_' . $vocab->vid;
        foreach ($langs as $langcode => $langname) {
          $lang_key = empty($langcode) ? $key : $key . '_' . $langcode;
          $form['taxonomy']['pattern'][$lang_key] = array() + $pattern_form_element;
          $form['taxonomy']['pattern'][$lang_key]['#title'] = t('%title', array('%title' => $vocab->name));
          $form['taxonomy']['scope'][$lang_key] = array('#type' => 'markup', '#value' => t('Taxonomy'));
          $form['taxonomy']['pattern'][$lang_key]['#default_value'] = variable_get($lang_key, '');
          $form['taxonomy']['language'][$lang_key] = array('#type' => 'markup', '#value' => t('%lang', array('%lang' => $langname)));
        }
      }
    }
  }

  // Token help
  _page_titles_per_language_add_tokens_help($form['taxonomy'], 'taxonomy');

  return $form;
}

/**
 * Initialisation of general patterns' settings.
 */
function _page_titles_per_language_init_general_patterns_options() {
  $general_patterns['page_title_front'] = array(
    '#title' => t('Frontpage'),
    '#scope' => t('Global'),
    '#langneutral' => 1
  );
  if (module_exists('profile')) {
    $general_patterns['page_title_user']=array(
      '#title' => t('User Profile'),
      '#scope' => 'User',
      '#showfield' => 1
    );
  };
  if (module_exists('blog')) {
    $general_patterns['page_title_blog']=array(
      '#title' => t('Blog'),
      '#scope' => 'User'
    );
  };
  if (module_exists('forum')) {
    $general_patterns['page_title_forum_root_title']=array(
      '#title' => t('Forum Root'),
      '#scope' => 'Global'
    );
  };
  $general_patterns['page_title_comment_reply'] = array(
      '#title' => t('Comment Reply'),
      '#scope' => t('Node')
  );
  $general_patterns['page_title_comment_child_reply'] = array(
    '#title' => t('Comment Child Reply'),
    '#scope' => t('Node Comment')
  );
  return $general_patterns;
}

/**
 * Add the tokens help to the form passed as argument.
 *
 * @param &$form
 *   The form to which the help text will be added.
 * @param $type
 *   An array containing information about the object for which the meta tags
 *   are being edited.
 */
function _page_titles_per_language_add_tokens_help(&$form, $type) {
  if (module_exists('token')) {
    // Always include the global context.
    $token_types[] = 'global';

    switch ($type) {
      case 'general':
        $token_types[] = 'user';
        $token_types[] = 'node';
        $token_types[] = 'comment';
        break;
      case 'types':
        $token_types[] = 'node';
        break;
      case 'taxonomy':
        $token_types[] = 'taxonomy';
        break;
    }

    $form[$type]['token_help']['help'] = array(
      '#value' => theme('token_tree', $token_types),
    );
  }
  else {
    return;
  }
}
