-- SUMMARY --
This module allows you to set page titles per language by using the "Page Title" logic and providing a clean interface.

-- REQUIREMENTS --
You must have the "Page Title" and "Locale" modules enabled.
 
 -- INSTALLATION --
Install as usual, see http://drupal.org/node/70151 for further information.
 
 -- CONFIGURATION --
Configure user permissions in Administration > User Management > Permissions.